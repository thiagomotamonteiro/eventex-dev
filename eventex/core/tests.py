from django.test import TestCase

class HomeTest(TestCase): #Classe de teste para nossa app core
    
    def setUp(self): #Função criada para que o o client seja usado em outras funções, evitando fepetição
        self.resp = self.client.get('/') # self.resp -> Variável, self.client.get('/') -> 'client' é o browser de navegação, o 'get('/)' é para pegar tudo na raiz
                    
    def test_get_status_code(self): # Função que testa se o retorno da requisição sta ok, com o código '200', cso contrário retornaria código de erro '400'
        """ Get / must return code 200 ok """        
        self.assertEqual(self.resp.status_code, 200)
        
    def test_template(self):# Funçã testa se o retorno é o template indicado
        """ Must use 'index.html' in / """
        self.assertTemplateUsed(self.resp, 'index.html')     
        
    
    