from django.shortcuts import render, get_object_or_404
#from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from eventex.core.models import Speaker, Talk, Course

# def home(request):
#     speakers = Speaker.objects.all()    
#     return render(request, 'index.html', {'speakers': speakers})

class home(ListView):# Lista todos os speakers do model Speaker -> É o mesmo que Speaker.objects.all()
    template_name = 'index.html'
    model = Speaker 
    context_object_name = 'speakers'# context_object_name define a variável de nome 'speakers' que é usada no 
                                         # index.html para listar speakers com laço for.
home = home.as_view() # home é instância de home(ListView) que é chamada na url da app core

# def speaker_detail(request, slug):
#     speaker = get_object_or_404(Speaker, slug=slug)
#     return render(request, 'core/speaker_detail.html', {'speaker':speaker})

class speaker_detail(DetailView):
    template_name = 'core/speaker_detail.html'
    model = Speaker
    context_object_name = 'speaker_detail'

speaker_detail = speaker_detail.as_view()

def talk_list(request):
    HORARIO_PALESTRA_CURSOS = Course()

    context = {
        #'morning_talks':Talk.objects.filter(start__lt='12:00'),
        'morning_talks': HORARIO_PALESTRA_CURSOS.get_morning_talks_courses(),
        'afternoon_talks': HORARIO_PALESTRA_CURSOS.get_afternoon_talks_courses(),
        #'courses':Course.objects.all(),
    }
    return render(request, 'core/talk_list.html', context)