from django.urls import re_path
from eventex.core.views import home, speaker_detail, talk_list

urlpatterns = [
    re_path(r'^$', home, name='home'),
    re_path(r'^palestrantes/(?P<slug>[^\.]+)/$', speaker_detail, name='speaker_detail'),
    re_path(r'^palestras/$', talk_list, name='talk_list'),

]
