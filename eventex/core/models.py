from django.db import models
from django.shortcuts import resolve_url as r

class Speaker(models.Model):
    name = models.CharField('Nome', max_length=255)
    slug = models.SlugField('Slug')
    photo = models.URLField('Foto')
    website = models.URLField('Website', blank=True)
    description = models.TextField('Descrição', blank=True)

    class Meta:
        verbose_name = 'Palestrante'
        verbose_name_plural = 'Palestratantes'
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return r('speaker_detail', slug=self.slug)# 'speaker_detail' -> é o nome da url no core/urls.py

class Contact(models.Model):
    EMAIL = 'E'
    PHONE = 'P'
    #KINDS É UM ATRIBUTO DE CLASSES (CONSTANTE)
    KINDS = (
        (EMAIL, 'Email'),
        (PHONE, 'Telefone'),
    ) 
    speaker = models.ForeignKey('Speaker', on_delete=models.CASCADE, verbose_name = 'palestrante')# 'Speaker'-> Modelo da clase que quero relacionar, 
                                                                    #on_delete-> Informa ao django como gerenciar a remoção do elemento na tabela. qUANDO DELETAR QUALQUER 'speaker'
                                                                    # ele deleta todos os contatos a ele relacionado.
    kind = models.CharField(max_length=1, choices=KINDS, verbose_name = 'tipo')#Tipo de contato
    value = models.CharField(max_length=255, verbose_name = 'valor')


    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'
    
    def __str__(self):
        return self.value

'''
A classe Talk e classe Course yem os mesmos atributos e métodos o que difere é que na classe Course possui um campo
a mais que é o slots. Contudo, criamos a herança de classa do modelo, para que as duas classes Talk e Course
herdem. Chamamos de herança de classe abstrata.

'''

class Activity(models.Model):
    title = models.CharField(max_length=200, verbose_name='título')
    start = models.TimeField(verbose_name='início', blank=True, null=True)
    description = models.TextField(verbose_name='descrição', blank=True)
    speakers = models.ManyToManyField('Speaker', verbose_name='palestrantes', blank=True)

    class Meta:
        abstract=True
        ordering = ['start']
        verbose_name = 'Palestra'
        verbose_name_plural = 'Palestras'

    def __str__(self):
        return self.title
#Métodos
    def get_morning_talks(self):# Pega todas as palestras da manhã
       qs = Talk.objects.filter(start__lt='12:00')
       return qs
        
    def get_afternoon_talks(self):#Pega todas as palestras da tarde
        qs = Talk.objects.filter(start__gte='12:00')
        return qs

class Talk(Activity): # Classe Talk herdado tudo da classe Activity
    pass

class Course(Activity):
    slots = models.IntegerField()

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

    def get_morning_talks_courses(self):
        morning_talks = self.get_morning_talks()
        qs_morning_courses = Course.objects.filter(start__lt='12:00') 
        morning_talks_courses = list(morning_talks) + list(qs_morning_courses) # União das duas queresets como listas
        morning_talks_courses.sort(key=lambda o: o.start)  # Usei a função lambda para ordenara a lista por horário 'start'. 
        # Obs: lambda é uma função anônimas. key=lambda desta forma é muito usado com sort ou sorted, para ordenar lista por meio de um objeto 
        # sort() - Um método que modifica a lista no local
        # sorted() - cria uma nova lista classificada
        return morning_talks_courses  # Retorna a variável da união das listas já ordenadas. 
        
    def get_afternoon_talks_courses(self):
        afternoon_talks = self.get_afternoon_talks()
        qs_afternoon_courses = Course.objects.filter(start__gte='12:00') 
        afternoon_talks_courses = list(afternoon_talks) + list(qs_afternoon_courses) #União das duas queresets como listas
        afternoon_talks_courses.sort(key=lambda o: o.start)  # Usei a função lambda para ordenara a lista por horário 'start'
        return afternoon_talks_courses # Retorna a variável da união das listas já ordenadas.

# class Talk(models.Model):
#     title = models.CharField(max_length=200, verbose_name='título')
#     start = models.TimeField(verbose_name='início', blank=True, null=True)
#     description = models.TextField(verbose_name='descrição', blank=True)
#     speakers = models.ManyToManyField('Speaker', verbose_name='palestrantes', blank=True)

#     class Meta:
#         verbose_name = 'Palestra'
#         verbose_name_plural = 'Palestras'

#     def __str__(self):
#         return self.title
# #Métodos
#     def get_morning_talks(self):# Pega todas as palestras da manhã
#        qs = Talk.objects.filter(start__lt='12:00')
#        return qs
        
#     def get_afternoon_talks(self):#Pega todas as palestras da tarde
#         qs = Talk.objects.filter(start__gte='12:00')
#         return qs

# class Course(models.Model):
#     title = models.CharField(max_length=200, verbose_name='título')
#     start = models.TimeField(verbose_name='início', blank=True, null=True)
#     description = models.TextField(verbose_name='descrição', blank=True)
#     slots = models.IntegerField()
#     speakers = models.ManyToManyField('Speaker', verbose_name='palestrantes')
    
#     class Meta:
#         verbose_name = 'Curso'
#         verbose_name_plural = 'Cursos'

#     def __str__(self):
#         return self.title
# #Métodos
#     def get_morning_talks(self):# Pega todas as palestras da manhã
#        qs = Course.objects.filter(start__lt='12:00')
#        return qs
        
#     def get_afternoon_talks(self):#Pega todas as palestras da tarde
#         qs = Course.objects.filter(start__gte='12:00')
#         return qs
