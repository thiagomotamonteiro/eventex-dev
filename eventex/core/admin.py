from django.contrib import admin
from django.utils.html import format_html
from eventex.core.models import Speaker, Contact, Talk, Course

class ContactInline(admin.TabularInline): #TabularInline-> Serve para permitir criar várias campos para colocar vários contatos. 
    model = Contact
    extra = 1 #Informa quantos campos aparece. Neste caso é um(01)

class SpeakerModelAdmin(admin.ModelAdmin):
    inlines = [ContactInline] # Mostra os campos da class ContactInline(admin.TabularInline): 
    prepopulated_fields = {'slug':('name',)} #Preenche automaticamente o campo 'slug' quando preenche o campo 'name'. 'slug': Campo a ser populado pelo campo ('name',)
    list_display = ['name', 'photo_img', 'website_link', 'get_email', 'get_phone']

    def website_link(self, obj): # Gera um link
        return format_html('<a href="{0}">{0}</a>', obj.website)
    website_link.short_description = 'website'

    def photo_img(self, obj):
        return format_html('<img width="32px" src="{}" />', obj.photo)
    photo_img.short_description = 'foto'

    def get_email(self,obj): #Pega o primeiro e-mail cadastrado do palestrante
        return Contact.objects.filter(kind=Contact.EMAIL, speaker=obj).first()
    get_email.short_description = 'e-mail' 
    
    def get_phone(self,obj): #Pega o primeiro telefone cadastrado do palestrante
        return Contact.objects.filter(kind=Contact.PHONE, speaker=obj).first()     
    get_phone.short_description = 'telefone'

admin.site.register(Speaker, SpeakerModelAdmin)
admin.site.register(Talk)
admin.site.register(Course)