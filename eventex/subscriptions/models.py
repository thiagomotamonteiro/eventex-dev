# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import default
from eventex.subscriptions.validators import validate_cpf

class Subscription(models.Model):
    
    name = models.CharField(max_length=100, verbose_name=u'Nome')
    cpf = models.CharField(max_length=14, verbose_name=u'CPF', validators=[validate_cpf])# Este campo possui um validador de cpf.
    email = models.EmailField(verbose_name=u'E-mail', blank=True)
    phone = models.CharField(max_length=20, verbose_name=u'Telefone', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Criado em')
    paid = models.BooleanField(default=False, verbose_name= u'Pago')
    
    class Meta:
        verbose_name = u'inscrição'
        verbose_name_plural = u'inscrições'
        ordering = ('-created_at',)        
    def __str__(self):
        return self.name