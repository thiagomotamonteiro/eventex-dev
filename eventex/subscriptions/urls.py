from django.urls import re_path
from eventex.subscriptions.views import subscribe, detail

app_name = 'subscriptions' #DEfinindo o namespace da url global
urlpatterns = [
    re_path(r'^$', subscribe, name='subscribe'),
    re_path(r'^(?P<pk>\d+)/$', detail, name='detail'),
]
