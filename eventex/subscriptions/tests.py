from django.test import TestCase
from eventex.subscriptions.forms import SubscriptionForm

class SubscriptionsTest(TestCase):

    def setUp(self):
        self.response = self.client.get('/inscricao/')

    def test_get(self):
        """ Get /inscricao/ must return status code 200 """        
        self.assertEqual(self.response.status_code, 200)

    def test_template(self):
        """ Must use app subscriptions/subscription_form.html """        
        self.assertTemplateUsed(self.response, 'subscriptions/subscription_form.html')
        
    def test_html(self):
        """ HTML must contain input tags """
        self.assertContains(self.response, '<form')# Quero que tenha um formulário no html
        self.assertContains(self.response, '<input', 6)# Quero que tenha 5 inputs no formulário. São eles: 4 campos e 01 botão e 01 csrf_token
        self.assertContains(self.response, 'type="text"', 3)# Quero que tenha 3 campos tipo texto no formulário: São eles nome, cpf, telefone.
        self.assertContains(self.response, 'type="email"')#Quero que tenha 1 campos tipo email no formulário.
        self.assertContains(self.response, 'type="submit"')#Quero que tenha 1 campos tipo submit no formulário. 

    def test_csrf(self):
        """ HTML must contein csrf """
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_has_form(self):
        """ context must have subscription form """
        form = self.response.context['form'] # Todo template é renderizado pelo contexto. É no contexto que fica as variáveis dinâmicas do template
        self.assertIsInstance(form, SubscriptionForm)
    
# O context funciona como um dicionário de contexto. 

    def test_form_has_fields(self):
        """ form must have 4 field """
        form = self.response.context['form']
        self.assertSequenceEqual(['name', 'cpf', 'email', 'phone'], list(form.fields))
        