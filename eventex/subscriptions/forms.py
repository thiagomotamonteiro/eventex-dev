# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from eventex.subscriptions.models import Subscription
from eventex.subscriptions.validators import validate_cpf

class SubscriptionFormOld(forms.Form):    
    
    name = forms.CharField(label='Nome')
    cpf = forms.CharField(label='CPF', widget=forms.TextInput(attrs={'placeholder': 'Somente números.'}), validators=[validate_cpf])
    email = forms.EmailField(label='Email', required=False)
    phone = forms.CharField(label='Telefone', required=False)
    
    """
    Vamos capitalizar o campo name para formatar os nomes da forma  qua 1ª letra seja
    Maiúscula e o restante seja minúscula.
    """
    
    def clean_name(self):
        name = self.cleaned_data['name']
        words = [w.capitalize() for w in name.split()]        
        return ' '.join(words)

    def clean(self): # o 'clean' é chamado depois que os campos do cleaned_data foram validade.
        if not self.cleaned_data.get('email') and not self.cleaned_data.get('phone'):
            raise ValidationError('Favor preencher o campo de e-mail ou telefone.')
        return self.cleaned_data

""" Gerando os campos do formulário através dos campos do próprio model subscription """    
class SubscriptionForm(forms.ModelForm):
    
    class Meta:
        model = Subscription #Definindo o modelo que será usado pelo SubscriptionsForm
        fields = ['name', 'cpf', 'email', 'phone'] # Define os campos que serão mostrado no formulário. É uma lista de strings
        #exclude = ['created_at', 'paid'] Omite os campos no formulário
        
    """
    Vamos capitalizar o campo name para formatar os nomes da forma  qua 1ª letra seja
    Maiúscula e o restante seja minúscula.
    """
    
    def clean_name(self):
        name = self.cleaned_data['name']
        words = [w.capitalize() for w in name.split()]        
        return ' '.join(words)
         
    def clean(self): # o 'clean' é chamado depois que os campos do cleaned_data foram validade.
        self.cleaned_data = super().clean()# Garante que vai chamar o clean do nosso modelForm.
        
        if not self.cleaned_data.get('email') and not self.cleaned_data.get('phone'):
            raise ValidationError('Favor preencher o campo de e-mail ou telefone.')
        return self.cleaned_data
         
         
         
         
         
         
         
         
         
            