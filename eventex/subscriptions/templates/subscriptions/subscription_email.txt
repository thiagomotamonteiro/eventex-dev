Olá, tudo bem?

Muito obrigada por se inscrever no Eventex.

Esses são os dados que você nos forneceu em sua inscrição:

Nome: {{ name }}
CPF: {{ cpf }}
Email: {{ email }}
Telefone: {{ phone }}

Em 48 horas úteis, nossa equipe entrará em contato
com você para concluirmos sua matrícula.

Atenciosamente,
--
Morena
