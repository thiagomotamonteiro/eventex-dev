# -*- coding: utf-8 -*-
from django.apps import AppConfig

class SubscriptionsConfig(AppConfig):
    name = u'eventex.subscriptions' #Nome da app
    verbose_name = u'Controle de participantes' # Nome que será aprensentado no admin
    