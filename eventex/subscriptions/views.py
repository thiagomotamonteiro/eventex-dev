from django.contrib import messages
from django.core import mail
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.template.loader import render_to_string
from django.conf import settings
from django.views.generic import DetailView
from eventex.subscriptions.forms import SubscriptionForm
from eventex.subscriptions.models import Subscription


# def subscribe(request):
#     if request.method == 'POST':
#         form = SubscriptionForm(request.POST)# Passando ao construtor do formulário(SubscriptionForm(request.POST)) os dados que vem da requisição
#         if form.is_valid():# Verifica se o formulário está preenchido correto e transforma as string em objetos python em alto nível. 'is_valid' chama internamente o método 'full_clean()'
#             body = render_to_string('subscriptions/subscription_email.txt',
#                                     form.cleaned_data)
#             # Vamos enviar o e-mail:0
#             mail.send_mail('Confirmação de Inscrição!', #Assunto
#                            body, #Mensagem(body-> variável que recebe o arquivo texto do email 'subscriptions/subscription_email.txt' .
#                            'agomottam@yahoo.com.br', #Quem envia o e-mail
#                            ['agomottam@yahoo.com.br', form.cleaned_data['email']])# Quem recebe o e-mail "é uma lista de destinatário "
#             messages.success(request, 'Inscrição realizada com sucesso!')
#             return HttpResponseRedirect('/inscricao/')
#         else:
#             return render(request, 'subscriptions/subscription_form.html',
#                           {'form':form})# Retorna a página de inscrição com os erros dos campos do formulário                
# #            return HttpResponse() # Retorna stutus code 200, caso o formulário não seja preenchido de forma válida            
#     else: # Cai aqui como se fosse o get
#         context = {'form':SubscriptionForm()} #SubscriptionForm() é uma classe por isso precis de '()'
#         return render(request, 'subscriptions/subscription_form.html', context)
def subscribe(request):
    if request.method == 'POST':
        return create(request)
    else:
        return new(request)
    
def create(request):  
    form = SubscriptionForm(request.POST)# Passando ao construtor do formulário(SubscriptionForm(request.POST)) os dados que vem da requisição

    if not form.is_valid():
        return render(request, 'subscriptions/subscription_form.html',
              {'form':form})# Retorna a página de inscrição com os erros dos campos do formulário               

    else:
        # Envia e-mail    
        template_name = 'subscriptions/subscription_email.txt'
        context = form.cleaned_data
        subject = 'Confirmação de Inscrição!'    
        from_ = settings.DEFAULT_FROM_EMAIL
        to = form.cleaned_data['email']
        body = render_to_string(template_name,
                                context)
        
        # Vamos enviar o e-mail:0
        mail.send_mail(subject, #Assunto
                       body, #Mensagem(body-> variável que recebe o arquivo texto do email 'subscriptions/subscription_email.txt' .
                       from_, #Quem envia o e-mail
                       [from_, to])# Quem recebe o e-mail "é uma lista de destinatário "
    
    # Salva os dados no banco OBS: Podemos salvar com o método 'save' ou direto no banco como mostra abaixo:
        #Subscription.objects.create(**form.cleaned_data)#Subscription-> É a classe do models.py,
                                           #**form-> Pega todos os campos do formulário para ser salvado
                                           # cleaned_data -> Valida os campos do formulário. 
        """
          A forma abaixo é para salvar dados no banco por meio de formulário. Form.Forms
        """   
#        # Para salvar no banco pegando os dados do formulário 'forms.form' pode ser de duas maneiras abaixo:    
#           
#        # subscription = Subscription.objects.create(**form.cleaned_data)
#         subscription = Subscription(name=form.cleaned_data['name'],
#                                    cpf=form.cleaned_data['cpf'].replace('.',"").replace('-',""),
#                                    email=form.cleaned_data['email'],
#                                    phone=form.cleaned_data['phone'].replace('(',"").replace(')',"").replace('-',""))
#         subscription.save()
#         return HttpResponseRedirect('/inscricao/{}'.format(subscription.pk))
        """
         A forma abaixo é para salvar no banco com modelforms.
        """
        subscription = form.save()  
        return HttpResponseRedirect('/inscricao/{}'.format(subscription.pk))
    # Mostra a mensagem de sucesso ao usuário
        #messages.success(request, 'Inscrição realizada com sucesso!')
        #return HttpResponseRedirect('/inscricao/')
  
def new(request): # Cai aqui como o get
#    context = {'form':SubscriptionForm()} #SubscriptionForm() é uma classe por isso precis de '()'
    return render(request, 'subscriptions/subscription_form.html', {'form':SubscriptionForm()})

# def detail(request, pk):
#     try:
#         subscription = Subscription.objects.get(pk=pk)
#     except Subscription.DoesNotExist:
#         raise Http404
#     return render(request, 'subscriptions/detail.html',
#                   {'subscription' : subscription})

class detail(DetailView):
    template_name = 'subscriptions/detail.html'
    model = Subscription
    context_object_name = 'subscription_detail'

detail = detail.as_view()
    
    