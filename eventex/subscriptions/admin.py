# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.timezone import now

#Importando o model 'Subscription' da app 'subscriptions' 
from eventex.subscriptions.models import Subscription 


class SubscriptionModelAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'email', 'phone', 'cpf', 'created_at', 'subscribed_today', 'paid')
    date_hierarchy = 'created_at' # Mostra as 1ª inscrições criadas pela hierarquia.
    search_fields = ('name', 'email', 'phone', 'cpf', 'created_at')# Buscar por campos com a sequencia de prioridades.
    list_filter = ('paid', 'created_at')
    
    actions = ['mark_as_paid'] # insere a opção de marcar como pago na caixa de ação do admin para o usuário.

    def subscribed_today(self,obj): #Informa se a inscrição foi feita hoje ou não
        return obj.created_at == now().date()

#         hoje = obj.created_at == now().date()
#         if hoje==True:
#             return u'sim'
#         return u'Não'
    
    def mark_as_paid(self, request, quereset):
        count = quereset.update(paid=True)
        
        if count == 1:
            msg = '{} inscrição foi marcada como paga.'
        else:
            msg = '{} inscrições foram marcadas com pagas.'
        
        self.message_user(request, msg.format(count))
                
    subscribed_today.short_description = u'Inscrito hoje?'
    subscribed_today.boolean = True # Se for inscrito hoje motra verde se não mostar vermelho.
    mark_as_paid.short_description = u'Marcar com pago'
#Registrando o model 'Subscription' no admin. Com isso peagmos todos os campos do model.
admin.site.register(Subscription, SubscriptionModelAdmin)