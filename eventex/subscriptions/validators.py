from django.core.exceptions import ValidationError

"""
Função que valida o campo de cpf no formulário que está 
sendo definido no campo 'cpf' no subscriptins/models
"""

def validate_cpf(value):
    """ Como estou usando uma formatação de campo no formulário 
    subscription_form.html com javascript não utilizarei o if not value.isdigit() abaixo  """
#     if not value.isdigit(): # se o valor digitato não for dígito "números', mortra msg de rro abaixo.
#         raise ValidationError(u'CPF deve conter apenas números. ')

    if len(value)<11:
        raise ValidationError(u'CPF precisa ter 11 números.')

